import json
import csv
import math
import time
from datetime import datetime, timedelta
import os


if __name__ == '__main__':
    

    words_path = "top_words_with_value.txt"
    output = open('restaurant_tfidf.json', 'w')

    restaurants = {}
    with open(words_path, 'r') as f:
        line = f.readline()
        line = line.rstrip()
        cnt = 1
        preName = None

        while line:
            if line.rstrip() == "":
                continue

            print("Line : {}".format(line))
            if cnt % 2 == 1:
                preName = line.strip()
                restaurants[preName] = []
            else:
                words = line.split(" ")
                for item in words:
                    if item == "\n":
                        continue
                    arr = item.split(":")
                    print(arr)
                    if preName:
                        restaurants[preName].append({ "text" : arr[0], "value": float(arr[1])})
            line = f.readline()
            cnt += 1      
    

    output.write(json.dumps(restaurants, indent=2))
    f.close()
    output.close()