import json
import csv
import math
import time
from datetime import datetime, timedelta
import os
from nltk.corpus import stopwords 

import nltk
nltk.download('stopwords')

if __name__ == '__main__':
    

    positive_path = "opinion-lexicon-English/positive-words.txt"
    negative_path = "opinion-lexicon-English/negative-words.txt"
    reviews = "new_reviews.csv"
    output = open('restaurant_sentiment.json', 'w')

    rst = []
    pos_words = set()
    neg_words = set()

    # Load stop words
    stop_words = stopwords.words('english')
    # print(stop_words[:5])

    # positive words
    posFiles = open(positive_path, 'r')
    lines = posFiles.readlines()

    for line in lines:
        line = line.strip('\n')
        line = line.strip('\t')

        if line != "":
            pos_words.add(line)

    # print("====== positive words ======")
    # print(pos_words)

    # negative words
    negFiles = open(negative_path, 'r')
    lines = negFiles.readlines()

    for line in lines:
        line = line.strip('\n')
        line = line.strip('\t')
        
        if line != "":
            neg_words.add(line)
    # print("====== negative words ======")
    # print(neg_words)


    with open(reviews, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        header = next(reader)

        for row in reader:
            restaurant = {}
            new_review = ""
            raw_name = row[0]
            raw_review = row[1]
            pos_cnts = {}
            neg_cnts = {}

            for i in range(len(raw_review)):
                if raw_review[i].isalnum():
                    new_review += raw_review[i]
                else:
                    new_review += " "

            tokens = new_review.split(" ")
            pos_total_cnt = 0
            neg_total_cnt = 0

            for i in range(len(tokens)):
                word = tokens[i].lower().strip()
                # print("token: ", word)
                if not word or word == "" or word == " ":
                    continue

                # print("token: ", word)
                
                if word in pos_words:
                    if word not in pos_cnts:
                        pos_cnts[word] = 0
                    pos_cnts[word] += 1
                    pos_total_cnt += 1

                if word in neg_words:
                    if word not in neg_cnts:
                        neg_cnts[word] = 0
                    neg_cnts[word] += 1
                    neg_total_cnt += 1
        
            restaurant["name"] = raw_name
            restaurant["positive_words"] = []
            restaurant["negative_words"] = []

            for word in pos_cnts:
                restaurant["positive_words"].append({
                    "name" : word,
                    "value" : pos_cnts[word] / (pos_total_cnt) * 100
                })
            for word in neg_cnts:
                restaurant["negative_words"].append({
                    "name" : word,
                    "value" : neg_cnts[word] / (neg_total_cnt) * 100
                })
            
            restaurant["positive_words"] = sorted(restaurant["positive_words"],  key = lambda k : k['value'], reverse = True)[:10]
            restaurant["negative_words"] = sorted(restaurant["negative_words"],  key = lambda k : k['value'], reverse = True)[:10]
            restaurant["positive_counts"] = pos_total_cnt
            restaurant["negative_counts"] = neg_total_cnt

            # print(restaurant)
            # for i in range(len(header)):
            #     key = header[i]
            #     value = row[i]
            #     restaurant[key] = value
            rst.append(restaurant)     


    output.write(json.dumps(rst, indent=4))
    f.close()
    output.close()


    with open('restaurant_rating.json') as json_file:
        data = json.load(json_file)

        for i, r1 in enumerate(rst):
            name = r1["name"]
            positive_words = r1["positive_words"]
            negative_words = r1["negative_words"]
            
            for j, r2 in enumerate(data):
                if r1["name"] == r2["name"]:
                    print(r1["name"])
                    data[j]["positive_words"] = positive_words
                    data[j]["negative_words"] = negative_words
                    data[j]["positive_counts"] = r1["positive_counts"]
                    data[j]["negative_counts"] = r1["negative_counts"]

    output = open('restaurant_rating.json', 'w')
    output.write(json.dumps(data, indent=4))
    output.close()
