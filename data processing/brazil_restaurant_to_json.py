import json
import csv
import math
import time
from datetime import datetime, timedelta
import os


if __name__ == '__main__':
    

    restaurant_path = "brazil_reviews.csv"
    output = open('brazil_restaurant_rating.json', 'w')
    with open('recommend_restaurants_l.json', 'r') as f:
        performance = json.load(f)
    print(performance)
    rst = []
    total_footprint = 0
    with open(restaurant_path, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        header = next(reader)

        for row in reader:
            restaurant = {}
            

            for i in range(len(header)):
                key = header[i]
                value = row[i]
                restaurant[key] = value

            if restaurant["name"] not in performance:
                continue;
                
            restaurant["performance"] = performance[restaurant["name"]]
            

            rst.append(restaurant)        
    
        
    output.write(json.dumps(rst, indent=2))
    f.close()
    output.close()