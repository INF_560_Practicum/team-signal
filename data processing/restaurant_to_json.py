import json
import csv
import math
import time
from datetime import datetime, timedelta
import os


if __name__ == '__main__':
    

    restaurant_path = "restaurant_rating.csv"
    weekday_traffic = "formatted_weekday_customer.csv"
    output = open('restaurant_rating.json', 'w')

    rst = []
    total_footprint = 0
    with open(restaurant_path, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        header = next(reader)

        for row in reader:
            restaurant = {}
            for i in range(len(header)):
                key = header[i]
                if key == "daily_footprints":
                    value = json.loads(row[i])
                else:
                    value = row[i]

                if key == "monthly_footprint":
                    total_footprint += int(value)
                restaurant[key] = value
            rst.append(restaurant)        
    
    for restaurant in rst:
        restaurant["total_monthly_footprint"] = total_footprint

    with open(weekday_traffic, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        header = next(reader)

        for row in reader:
            name = row[0]

            for i in range(len(rst)):
                restaurant = rst[i]
                # print(restaurant)
                if restaurant["name"] == name:
                    print(row[1:])
                    footprintArr = list(map(lambda x : int(x), row[1:]))
                    restaurant["daily_footprints"] = footprintArr


    output.write(json.dumps(rst, indent=4))
    f.close()
    output.close()