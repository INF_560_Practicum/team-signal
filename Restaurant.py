class Restaurant:
    _name = ""
    _latitude = 0.0
    _longitude = 0.0
    _area = 0.0
    _building = 0
    _level = 0

    def __init__(self, name, latitude, longitude, area, terminal, level):
        self._name = name
        self._latitude = latitude
        self._longitude = longitude
        self._area = area
        self._terminal = terminal
        self._level = level

    def name(self):
        return self._name

    def latitude(self):
        return self._latitude

    def longitude(self):
        return self._longitude

    def area(self):
        return self._area

    def terminal(self):
        return self._terminal

    def level(self):
        return self._level
