# Team Signal

## Project Description 
According to Rio airport report in 2019, the airport was rated poorly in many aspects especially in restaurants. Most of the complaints were about bad performance at those restaurants in the airport. It has the lowest rating on ‘Cost-effective stores and food courts’ and ‘Variety and quality of stores and food courts’ among all the categories in the Passenger Satisfaction Survey from the Rio airport website

our project focus on enhancing customer satisfaction and airport’s profitability by identifying unpopular restaurants inside the airport and searching better ones throughout Brazil. Multiple classification models will be built to predict the performance of the restaurants and BERT will be used to perform sentiment analysis, and a web application will be developed to visualize the restaurant performance, sentiment analysis and suggestions based on the airport map

## Files/Folder Description:
- `web code` : the web application implemented through React.js, Redux, Google Map to visualize restaurant performance, restaurant reviews, sentiment analysis and restaurant suggestions.
- `data processing` : scripts to parse the machine learning result into the format used in web application
- `Restaurant.py` : restaurant class model
- `Sentiment_Analysis_BERT.ipynb` and `Sentiment_Analysis_BERT.py`: a side service uses BERT model to train and test Yelp Review Polarity in order to predict the sentiment polarity of collected reviews at the RIO airport. 
- `data_preprocess.py` : scripts to parse and process Kiana terminal data to get customer traffic footpritn
- `feature_extraction+models+suggestion.py` and `feature_extraction+models+suggestion.ipynb` : our main service, which uses the customer traffic result to train different classification models, including SVM, Logistic Regression, Naive Bayes, CNN, TF-IDF, Word2vec, KNN, T-SNE. It will finally output the suggestions list of new restaurants.

## Web Demo
- Users can see the ratings and number of reviews from social media
- Users can see the customer footprint percentage of the selected restaurant
- The total customer footprint percentage is also broken down into a weekday statistic chart. Users can see how many customers stop by on each day
- Users can see the sentiment analysis from the collected reviews. They can get an overview of restaurant by seeing the ratio of positive and negative reviews
- Users can now see a suggestion restaurant list that are predicted to have better performance

![Restaruant Evaluation](./assets/Restaruant_Evaluation.png)

## Document
[Final Report](./assets/Final_report_team_signal.pdf)

[Final Presentation ](./assets/Final_Presentation_team_signal.pdf)
