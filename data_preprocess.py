import json
import csv
import math
import time
from datetime import datetime
from pyspark import SparkContext, SparkConf
import os
import Restaurant


class RestaurantSuggestion:
    RESTAURANT_LOCATION_PATH = "../locations.csv"
    RESTAURANT_RATING_PATH = "../ratings.csv"

    def __init__(self):
        pass

    def output_csv(self, name_to_rating):
        output = "\n"
        for name, rating in name_to_rating.items():
            output += str(name) + ", " + str(rating) + "\n"
        with open(self.RESTAURANT_RATING_PATH, 'a', encoding='utf-8') as output_file:
            output_file.write(output)
        output_file.close()

    def writer(self, iterator):
        date_path = "../date_t2/"
        date = iterator[0]
        for row in iterator[1]:
            if json.loads(row)['Building'] != "TPS2":
                continue
            file_name = os.path.join(date_path, str(date))
            if not os.path.exists(date_path):
                os.makedirs(date_path)
            with open(file_name, 'a', encoding='utf-8') as output_file:
                output_file.write(row + "\n")
            output_file.close()
        return iterator

    def groupby_date(self, input_path, sc):
        for filename in os.listdir(input_path):
            if filename == ".DS_Store":
                continue
            file_path = os.path.join(input_path, filename)
            inputs = sc.textFile(file_path).coalesce(12)

            time_to_row = inputs.map(lambda s: (json.loads(s)['localtime'], s))
            date_to_row = time_to_row.map(lambda line: (datetime.strptime(line[0][:10], '%Y-%m-%d').date(), line[1]))
            date_grouped = date_to_row.groupByKey().map(self.writer)
            print(date_grouped.count())
        # collect_out = date_grouped.collect()

    def get_time_different(self, iterator):
        earliest = datetime.now()
        latest = datetime.now()
        for i in iterator[1]:
            if earliest == 0:
                earliest = i
                latest = i
            if i < earliest:
                earliest = i
            elif i > latest:
                latest = i
            # if iterator[0] == 'f0:d7:aa:b4:ca:c8':
            #     detal = latest - earliest
            #     print("--------")
            #     print(iterator[0], earliest, latest, detal.seconds/60)
        detal = latest - earliest
        # print(iterator[0])

        return (iterator[0], detal.seconds / 60)

    def in_range_morethan5(self, restaurant, iterator):
        earliest = datetime.now()
        latest = datetime.now()
        for time_lat_lng_lvl in iterator:
            # print(time_lng_lat)
            time = time_lat_lng_lvl[0]
            # print(restaurant.latitude(), float(time_lat_lng_lvl[1]), restaurant.longitude(), float(time_lat_lng_lvl[2]))
            distance = math.sqrt((restaurant.longitude() - float(time_lat_lng_lvl[2])) ** 2 + (
                        restaurant.latitude() - float(time_lat_lng_lvl[1])) ** 2)
            # print(distance, restaurant.area())
            # and (restaurant.level() == 0 or restaurant.level() == int(time_lat_lng_lvl[3]))
            if distance < restaurant.area() and (restaurant.level() == 0 or restaurant.level() == int(time_lat_lng_lvl[3])):
                if earliest == 0:
                    earliest = time
                    latest = time
                if time < earliest:
                    earliest = time
                elif time > latest:
                    latest = time
        # # print(distance)
        detal = (latest - earliest).seconds / 60
        if detal < 5:
            # print("False")
            return False
        else:
            return True

    def get_restaurants(self):
        restaurants_t1 = dict()
        restaurants_t2 = dict()
        with open(self.RESTAURANT_LOCATION_PATH, 'r', encoding='utf-8', errors='ignore') as csvfile:
            readCSV = csv.DictReader(csvfile, delimiter=',')
            for line in readCSV:
                restaurant = Restaurant.Restaurant(line['restaurants'], float(line['lat']), float(line['lng']),
                                                   float(line['area']), int(line['terminal']), int(line['level']))

                if restaurant.terminal() == 1:  # in terminal 1
                    restaurants_t1[restaurant.name()] = restaurant  # name : (name lat lng dist building level)
                if restaurant.terminal() == 2:
                    restaurants_t2[restaurant.name()] = restaurant  # name : (name lat lng dist building level)
        csvfile.close()
        return restaurants_t1, restaurants_t2

    def start(self):
        start_time = time.time()

        input_path = "data_t2/"
        # input_path = "test/"

        sc = SparkContext("local[*]")
        sf = SparkConf()
        sf.set("spark.executor.memory", "10g")
        # pre-process the data_t1 to group by date
        # groupby_date(input_path)

        # get restaurants_t1 and restaurants_t2
        # restaurant_path = "restaurant_location.csv"
        restaurants_t1, restaurants_t2 = self.get_restaurants()
        # print(restaurants_t1)
        # print(restaurants_t2)
        restaurant_to_rating = dict()
        for month in range(9, 10, 1):
            if len(str(month)) == 1:
                month = "0" + str(month)
            else:
                month = str(month)

            for day in range(7, 8, 1):
                if len(str(day)) == 1:
                    day = "0" + str(day)
                else:
                    day = str(day)
                date = "2019-" + month + "-" + str(day)
                # TODO date_t1 or date_t2
                data_path = os.path.join("date_t2", date)
                inputs = sc.textFile(data_path).coalesce(12)

                # get rid of employee
                id_to_time = inputs.map(lambda x: (json.loads(x)['ClientMacAddr'],
                                                   datetime.strptime(json.loads(x)['localtime'][11:19],
                                                                     '%H:%M:%S'))).groupByKey()
                collect_out = id_to_time.map(self.get_time_different).filter(lambda x: x[1] <= 480).map(lambda x: x[0])
                # print("visitors: ", collect_out.count())
                visitors = set()  # id
                for visitor in collect_out.collect():
                    visitors.add(visitor)

                # get record for that day (ClientMacAddr, (time, lng, lat)) with only visitors
                id_to_location = inputs.map(lambda x: (json.loads(x)['ClientMacAddr'],
                                                       (
                                                       datetime.strptime(json.loads(x)['localtime'][11:19], '%H:%M:%S'),
                                                       json.loads(x)['lat'], json.loads(x)['lng'],
                                                       json.loads(x)['Level'][-1])))\
                    .filter(lambda x: x[0] in visitors).groupByKey()
                id_to_time_location = id_to_location.collect()  # (id, (time, lat, lng, lvl))

                # get how many people in each restaurant
                # TODO change restaurant terminal
                for restaurant in restaurants_t2:
                    for pairs in id_to_time_location:
                        # person = pairs[0]
                        if self.in_range_morethan5(restaurants_t2[restaurant], pairs[1]):
                            if restaurant not in restaurant_to_rating:
                                restaurant_to_rating[restaurant] = 0
                            restaurant_to_rating[restaurant] += 1

                print(restaurant_to_rating)

        self.output_csv(restaurant_to_rating)

        # json.dump(output, output_file)
        end_time = time.time()
        total_time = end_time - start_time
        print("total_time: ", total_time)


def main():
    rs = RestaurantSuggestion()
    try:
        rs.start()
    except Exception as e:
        raise e


if __name__ == '__main__':
    main()
